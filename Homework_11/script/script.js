// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. Додати до класу метод для виведення імені та прізвища

class User {
    constructor(firstName, secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
    show() {
        return console.log(`Hello, ${this.firstName} ${this.secondName}`)
    }
}

const user1 = new User('Maxim', 'Gres')

user1.show();

// Створи список, що складається з 4 аркушів. Використовуючи джс зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний

const ul = document.createElement('ul');

document.body.append(ul);

for (i = 0; i < 4; i++) {
    ul.insertAdjacentHTML('beforeend', `<li></li>`)
}

document.querySelector('ul>li').classList.add('blue');
document.querySelector('li:nth-child(3)').classList.add('red');

// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. При наведенні мишки виведіть текст координати, де знаходиться курсор мишки

const div1 = document.createElement('div');

div1.classList.add('div1');

document.body.append(div1);

div1.addEventListener('mousemove', (e) => {
    e.target.textContent = `X: ${e.clientX} Y: ${e.clientY}`;
})

div1.addEventListener('mouseout', (e) => {
    e.target.textContent = '';
})

// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута

const buttons = document.createElement('div');

const btn1 = document.createElement('input');
const btn2 = document.createElement('input');
const btn3 = document.createElement('input');
const btn4 = document.createElement('input');

btn1.type = 'button';
btn2.type = 'button';
btn3.type = 'button';
btn4.type = 'button';

btn1.value = '1';
btn2.value = '2';
btn3.value = '3';
btn4.value = '4';

btn1.classList.add('button');
btn2.classList.add('button');
btn3.classList.add('button');
btn4.classList.add('button');

document.body.append(buttons);

buttons.append(btn1);
buttons.append(btn2);
buttons.append(btn3);
buttons.append(btn4);

buttons.addEventListener('click', (e) => {
    if (!e.target.classList.contains('button')) return
    console.log(`Clicked ${e.target.value} button`)
})

// Створи див і зроби так, щоб при наведенні на див див змінював своє положення на сторінці

const innerDiv = document.createElement('div');
const outerDiv = document.createElement('div');

outerDiv.classList.add('outer-div');
innerDiv.classList.add('inner-div');

document.body.append(outerDiv);
outerDiv.append(innerDiv);

innerDiv.addEventListener('mouseover', (e) => {
    innerDiv.classList.add('div-move');
})

innerDiv.addEventListener('mouseout', (e) => {
    innerDiv.classList.remove('div-move');
})

// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body

const colorSelector = document.createElement('input');
colorSelector.type = 'color';
colorSelector.classList.add('color-selector')
document.body.append(colorSelector);

colorSelector.addEventListener('change', (e) => {
    document.body.style.background = `${e.target.value}`
})

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль

const inputLogin = document.createElement('input');
document.body.append(inputLogin);

inputLogin.addEventListener('input', (e) =>{
    console.log(e.target.value)
})