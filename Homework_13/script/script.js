// Выполнить запрос на https://swapi.dev/api/people получить список героев звездных войн.

//Вывести каждого героя отдельной карточкой с указанием. Имени, половой принадлежности, рост, цвет кожи, 
//год рождения и планету на которой родился.

// Создайте кнопку сохранить на каждой карточке. При нажатии кнопки записшите информацию в браузере

// Возможные свойства  :
// name строка - Имя этого человека.
// birth_year строка - Год рождения человека в соответствии со вселенскими стандартами ДБЯ или ПБЯ - до битвы при Явине или после битвы при Явине. Битва при Явине - это битва, которая происходит в конце эпизода IV «Звездных войн»: Новая надежда.
// eye_color строка - Цвет глаз этого человека. Будет "неизвестно", если неизвестно, или "н / д", если у человека нет глаза.
// gender строка - Пол этого человека. Либо «Мужской», «Женский», либо «Неизвестный», «н / д», если у человека нет пола.
// hair_color строка - Цвет волос этого человека. Будет "неизвестно", если неизвестно, или "н / п", если у человека нет волос.
// height строка - Рост человека в сантиметрах.
// mass строка - Масса человека в килограммах.
// skin_color строка - Цвет кожи этого человека.
// homeworld строка - URL ресурса планеты, планеты, на которой этот человек родился или населяет.
// films array - массив URL-адресов киноресурсов, в которых был этот человек.
// species array - массив URL-адресов ресурсов видов, к которым принадлежит этот человек.
// starships array - Массив URL-адресов ресурсов звездолета, которые пилотировал этот человек.
// vehicles array - массив URL-адресов ресурсов транспортного средства, которые пилотировал этот человек.
// url строка - URL-адрес гипермедиа этого ресурса.
// created строка - формат даты ISO 8601 времени создания этого ресурса.
// edited строка - формат даты ISO 8601 времени, когда этот ресурс редактировался.

// Для создания карточек испозуйте классы

class Hero {
    constructor(name, gender, height, skinColor, birthYear, homeworld) {
        this.name = name;
        this.gender = gender;
        this.height = height;
        this.skinColor = skinColor;
        this.birthYear = birthYear;
        this.homeworld = homeworld;
    }
    addCard() {
        const card = document.createElement('div');
        card.classList.add('card')
        card.insertAdjacentHTML('beforeend', `
            <p class="text">Name: <span>${this.name}</span></p>
            <p class="text">Gender: <span>${this.gender}<span></p>
            <p class="text">Height: <span>${this.height}</span></p>
            <p class="text">Skin color: <span>${this.skinColor}</span></p>
            <p class="text">Birth year: <span>${this.birthYear}</span></p>
            <p class="text">Homeworld: <span>${this.homeworld}</span></p>
            <input class="save-btn" type="button" value="Save">
            `)
        document.body.append(card);
    }

}

const data = fetch('https://swapi.dev/api/people')
const data1 = data.then((item) => { return item.json() });

data1.then((info) => {
    const res = info.results;
    res.forEach((el) => {
        const { name, gender, height, skin_color, birth_year, homeworld } = el
        const homeworldF = fetch(homeworld).then((item) => { return item.json() })
        homeworldF.then((info) => {
            const planetName = info.name
            new Hero(name, gender, height, skin_color, birth_year, planetName).addCard();
        })
    });
})

localStorage.heroes = JSON.stringify([]);

document.body.addEventListener('click', (e) => {
    if (!e.target.classList.contains('save-btn')) return
    const card = e.target.parentElement;
    const [...cardChild] = card.children;
    const [...cardChildValue] = cardChild
    .map((el) => el.children[0])
    .filter((el => el !== undefined))
    .map((el) => el.innerText)
    const storage = JSON.parse(localStorage.heroes);
    storage.push(new Hero(...cardChildValue));
    localStorage.heroes = JSON.stringify(storage);
    alert('Героя збережено в Local Storage');
})
