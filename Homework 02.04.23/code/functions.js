export const validate = (p, v) => p.test(v);

export function creatHtmlElem (tagName = "div", className,text = '', tadId, value = "" ){
  const element = document.createElement(tagName);
  if(className){
    element.classList.add(className);
  }
  if(tadId){
    element.id = tadId;
  }
  element.innerText = text;
  element.value = value;
  return element
}

export function createHtmlElements (tagName = "div", className, value = "", attr = [], listener, context) {
  const element = document.createElement(tagName);
  if(className){
      element.classList.add(className);
  }
  element.innerHTML = value;
  attr.forEach((obj)=>{
      const objectInfo = Object.entries(obj);
      element.setAttribute(objectInfo[0][0], objectInfo[0][1])
  })

  if("inputtextariaselect".includes(tagName)){
      if(!listener) return element
      element.addEventListener("change", (e) => {
          if(context){
             listener(context) 
          }else{
              listener(e)
          }
      })
  }else{
      element.addEventListener("click", (e) => {
          if(!listener) return element
          if(context){
             listener(context) 
          }else{
              listener(e)
          }
      })
  }
  return element
}

export function errorWindow (text,time = '2000'){
  const el = document.createElement('div');
  el.className = "error-box";
  el.innerHTML = `<p>${text}</p>`
  document.body.prepend(el);
  setTimeout(()=>{
    el.remove(el)
  },time);
}
