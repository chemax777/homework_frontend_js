import { creatHtmlElem, errorWindow, validate } from "./functions.js";

let tags = [];
let im = 'tagName';
let numAttachments = 1;
let numIdElem = 1;
let str = '';
let strTag = '';

const tag = {
  numAttachment: '',
  tagName: '',
  className: '',
  tagId: '',
  tagText: '',
  quantity: '',
};

const patern = "a div abbr article aside audio button section main header footer form h1 h2 h3 h4 h5 h6 img input ul li nav ol p span table tbody td th thead tfoot tr textarea label select map address canvas caption datalist hr iframe";

function showWindowResult(tag1, tag2 = '', tag3 = '') {
  let elm1 = '';
  let elm2 = '';
  let elm3 = '';

  for (let i = 0; i < tag1.quantity * 1; i += 1) {
    elm1 = creatHtmlElem(tag1.tagName, tag1.className, tag1.tagText, tag1.tagId);
    for (let a = 0; a < tag2.quantity * 1; a += 1) {
      if (tag2 !== '') {
        elm2 = creatHtmlElem(tag2.tagName, tag2.className, tag2.tagText, tag2.tagId);
        elm1.append(elm2);
      }
      for (let b = 0; b < tag3.quantity * 1; b += 1) {
        if (tag3 !== '') {
          elm3 = creatHtmlElem(tag3.tagName, tag3.className, tag3.tagText, tag3.tagId);
          elm2.append(elm3);
        }
      }
    }
    document.getElementById('window-result').append(elm1);
  }
}

function resultShow(arr) {
  let a = '';
  let b = '';
  let c = '';
  let d = '';
  arr.forEach((el, i) => {
    if (el.numAttachment * 1 === 0) {
      if (i < 2) {
        showWindowResult(el);
      }
      else d = el;
    }
    else if (el.numAttachment * 1 === 1) {
      a = el;
    }
    else if (el.numAttachment * 1 === 2) {
      b = el;
    }
    else if (el.numAttachment * 1 === 3) {
      c = el;
    }
  });
  if (d === '') {
    showWindowResult(a, b, c);
  }
  else {
    showWindowResult(a, b, c);
    showWindowResult(d);
  }
}

function cleaning() {
  tag.numAttachment = '',
    tag.tagName = '';
  tag.className = '';
  tag.tagId = '';
  tag.tagText = '';
  tag.quantity = '';
}

class Tags {
  constructor({ numAttachment, tagName, className, tagId, tagText, quantity }) {
    if (numAttachment === '') { this.numAttachment = 0 } else this.numAttachment = numAttachment;
    if (tagName === '' || !patern.includes(tagName.toLowerCase()) || validate(/[0-9]/, tagName)) {
      errorWindow(`${tagName} - Навірний тег!`, 4000);
      this.tagName = 'div';
    } else this.tagName = tagName.toLowerCase();
    this.className = className;
    this.tagId = tagId;
    this.tagText = tagText;
    if (quantity === '') { this.quantity = 1 } else this.quantity = quantity;
  }
}

function handlerResult(str) {
  if (str === '') {
    return
  };
  let arr = str.split('');
  im = 'tagName';
  arr.forEach((el, i) => {
    if (el === '.') {
      im = 'className';
      return
    }
    else if (el === '#') {
      im = 'tagId';
      return
    }
    else if (el === '{') {
      im = 'tagText';
      return
    }
    else if (el === '}') {
      im = 'tagName';
      return
    }
    else if (el === '*') {
      im = 'quantity';
      return
    }
    else if (el === '>') {
      im = 'tagName';
      if (numIdElem < 3) {
        tag.numAttachment = numIdElem;
        tags.push(new Tags(tag));
        cleaning()
        numAttachments += 1;
        numIdElem += 1;
        return;
      }
      else {
        arr.splice(i, arr.length - i);
        errorWindow("Перевищена кількість вкладеностей 3, Зайве буде видалено!");
        return
      }
    }
    else if (el === '^') {
      im = 'tagName';
      if (numAttachments < 3) {
        if (tags.length === 0) {
          tag.numAttachment = 0;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        else if (tags.length === 1 && tags[0].numAttachment === 0) {
          tag.numAttachment = 0;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        else {
          tag.numAttachment = numIdElem;
          tags.push(new Tags(tag));
          cleaning()
          numAttachments += 1;
          numIdElem = 1;
        }
        return;
      }
      else {
        arr.splice(i, arr.length - i);
        errorWindow("Перевищена кількість вкладеностей 3. Зайве буде видалено!");
        return
      }
    }
    else tag[im] += el;
  });
  if (tags.length === 2 && numIdElem === 1) {
    im = 'tagName'
    tag.numAttachment = 0;
    tags.push(new Tags(tag));
    cleaning()
    numAttachments = 1;
    numIdElem = 1;
    resultShow(tags);
    tags = [];
  }
  else {
    im = 'tagName'
    tag.numAttachment = numIdElem;
    tags.push(new Tags(tag));
    cleaning();
    numAttachments = 1;
    numIdElem = 1;
    resultShow(tags);
    tags = [];
  }
};


document.getElementById('window-input').addEventListener('keydown', (ev) => {
  if (ev.key === 'Enter') {
    str = ev.target.value.split(/\r\n|\r|\n/g);
    strTag = str[str.length - 1].trim();
    handlerResult(strTag);
  }
});


document.getElementById('window-input').addEventListener('change', (ev) => {
  if (strTag === "") {
    return
  }
  else {
    str = ev.target.value.split(/\r\n|\r|\n/g);
    strTag = str[str.length - 1].trim();
    handlerResult(strTag)
  }
});

