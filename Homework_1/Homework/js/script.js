var x = 6;
var y = 14;
var z = 4;
x += y - x++ * z

/*  1) x * z = 6 * 4 = 24
    2) y - 24 = 14 - 24 = -10
    3) x += 6-10 = -4
 */

document.write("x = " + x + "<br/>");

x = 6;
y = 14;
z = 4;
z = --x - y * 5
/*  1) --x = 6 - 1 = 5
    2) y * 5 = 14 * 5 = 70
    3) --x - y = 5 - 70 = -65
*/
document.write("z = " + z + "<br/>");

x = 6;
y = 14;
z = 4;
y /= x + 5 % z;
/*  1) 5 % z = 5 % 4 = 1 
    2) x + 1 = 6 + 1 = 7
    3) y /= 14 / 7 = 2
*/
document.write("y = " + y + "<br/>");

x = 6;
y = 14;
z = 4;
var rez = z - x++ + y * 5;
document.write("rez = " + rez + "<br/>");
/*  1) y * 5 = 14 * 5 = 70
    2) z - x = 4 - 6 = -2
    3) -2 + 70 = 68
 */

x = 6;
y = 14;
z = 4;
x = y - x++ * z;
/*  1) x * z = 6 * 4 = 24
    2) y - 24 = 14 - 24 = -10 
    3) x = -10
*/
document.write("x = " + x + "<br/>");
                        