/*
Використовуючи циклічні конструкції, пробіли (&nbsp;) та зірки (*) намалюйте:
Порожній 
- прямокутник 
Заповнений 
- Рівнобедрений трикутник 
- Трикутник прямокутний
- Ромб
 */

//Порожній прямокутник
for (let i = 0; i < 15; i++) {
    if (i == 0 || i == 14) {
        for (let j = 0; j < 10; j++) {
            document.write('*')
        }
    } else {
        for (let y1 = 1; y1 > 0; y1--) {
        document.write ('*')
        }
        for (let j = 1; j < 17; j++){
            document.write('&nbsp;')
        }
        for (let y1 = 17; y1 < 18; y1++) {
        document.write ('*')
        }
    }
    document.write('<br>');
}

// Заповнений рівнобедрений трикутник

for(let i = 0; i < 15; i++){
    for(let j = i + 1; j < 15; j++){
        document.write("&nbsp;")
    }
    for(let y = 1+i; y > 0; y--){
        document.write("*")
    }
    document.write("<br>")
}

// Заповнений прямокутний трикутник
for(let i = 0; i < 15; i++){
    for(let y = 1+i; y > 0; y--){
        document.write("*")
    }
    document.write("<br>")
}

// Заповнений ромб
for(let i = 0; i < 15; i++){
    for(let j = i + 1; j < 15; j++){
        document.write("&nbsp;")
    }
    for(let y = 1+i; y > 0; y--){
        document.write("*")
    }
    document.write("<br>")
}
for (let i = 14; i > 0; i--) {
    for (let j = i; j < 15; j++) {
        document.write("&nbsp;")
    }
    for (let y = i; y > 0; y--) {
        document.write("*")
    }
    document.write("<br>")
}