import React from 'react';
import ReactDOM from 'react-dom';

const Month = function () {
    return (
        <div>
            <h2>Місяці</h2>
            <ul>
                <li>Січень</li>
                <li>Лютий</li>
                <li>Березень</li>
                <li>Квітень</li>
                <li>Травень</li>
                <li>Червень</li>
                <li>Липень</li>
                <li>Серпень</li>
                <li>Вересень</li>
                <li>Жовтень</li>
                <li>Листопад</li>
                <li>Грудень</li>
            </ul>
        </div>
    )
}

const Week = function () {
    return (
        <div>
            <h2>Дні тижня</h2>
            <ul>
                <li>Понеділок</li>
                <li>Вівторок</li>
                <li>Середа</li>
                <li>Четвер</li>
                <li>П'ятниця</li>
                <li>Субота</li>
                <li>Неділя</li>
            </ul>
        </div>
    )
}

const Zodiac = function () {
    return (
        <div>
            <h2>Знаки зодіака</h2>
            <ul>
                <li>Овен</li>
                <li>Телець</li>
                <li>Близнюки</li>
                <li>Рак</li>
                <li>Лев</li>
                <li>Діва</li>
                <li>Терези</li>
                <li>Скорпіон</li>
                <li>Змієносець</li>
                <li>Стрілець</li>
                <li>Козоріг</li>
                <li>Водолій</li>
                <li>Риби</li>
            </ul>
        </div>
    )
}

const Lists = function () {
    return (
        <div>
            <Month></Month>
            <Week></Week>
            <Zodiac></Zodiac>
        </div>
    )
}

ReactDOM.render(<Lists></Lists>, document.querySelector('#root'))

