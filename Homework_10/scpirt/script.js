// В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
// * При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
// * При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
// * Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
// * При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.

const button = document.querySelector('.keys');
const display = document.querySelector('.display>input');
const digits = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.'];
const signs = ['-', '+', '*', '/'];
let num1 = '';
let num2 = '';
let sign = '';
let finish = false;
let memory = '';
let pressM = 0;
let pressMrc = 0;
let res = '';
let prevSign = '';
let multCalc = true;


display.value = '0'

button.addEventListener('click', function (e) {

    let key = e.target.value;

    if (!e.target.classList.contains('button')) return;

    if (digits.includes(key)) {
        if (num2 === '' && sign === '') {
            num1 += key;
            display.value = num1;
        } else if (num1 !== '' && num2 !== '' && finish) {
            num2 = key;
            finish = false;
            display.value = num2;
        } else {
            num2 += key;
            display.value = num2;
        }
        pressMrc = 0;
    }

    if (num1 !== '' && num2 !== '' && sign !== '' && signs.includes(key) && multCalc) {
        prevSign = sign;
        sign = key;
        if (prevSign === '+') {
            res = parseFloat(num1) + parseFloat(num2)
        } else if (prevSign === '-') {
            res = parseFloat(num1) - parseFloat(num2)
        } else if (prevSign === '*') {
            res = parseFloat(num1) * parseFloat(num2)
        } else if (prevSign === '/') {
            if (num2 === '0') {
                display.value = 'Ошибка!'
                num1 = '';
                num2 = '';
                sign = '';
                prevSign = '';
                return;
            }
            res = parseFloat(num1) / parseFloat(num2)
        }
        num1 = res;
        display.value = num1;
        finish = true;
        pressMrc = 0;
    }

    if (signs.includes(key)) {
        sign = key
        pressMrc = 0;
    }

    if (key === '=') {
        if (sign === '+') {
            num1 = parseFloat(num1) + parseFloat(num2)
        } else if (sign === '-') {
            num1 = parseFloat(num1) - parseFloat(num2)
        } else if (sign === '*') {
            num1 = parseFloat(num1) * parseFloat(num2)
        } else if (sign === '/') {
            if (num2 === '0') {
                display.value = 'Ошибка!'
                num1 = '';
                num2 = '';
                sign = '';
                return;
            }
            num1 = parseFloat(num1) / parseFloat(num2)
        }
        display.value = num1;
        finish = true;
        pressMrc = 0;
        multCalc = false;
    }


    if (key === 'C') {
        document.querySelector('.orange').setAttribute('disabled', 'disabled')
        display.value = '0'
        num1 = ''
        num2 = ''
        sign = ''
        prevSign = ''
        finish = false;
        multCalc = true;
        pressMrc = 0;
    }

    if (key === 'm+') {
        pressM++;
        memory = display.value;
        if (pressM <= 1) {
            document.querySelector('.display').insertAdjacentHTML('afterbegin', `<label>m</label>`)
        } else {
            return;
        }
        pressMrc = 0;
    }

    if (key === 'm-') {
        memory = '';
        document.querySelector('.display label').remove();
        pressM = 0;
        pressMrc = 0;
    }

    if (key === 'mrc') {
        pressMrc++;
        if (pressMrc <= 1) {
            if (num1 === '' && num2 === '') {
                num1 = memory;
                display.value = memory;
            } else {
                num2 = memory;
                display.value = memory;
            }
        } else {
            memory = '';
            document.querySelector('.display label').remove();
            pressM = 0;
            pressMrc = 0;
        }
    }

    if (num1 !== '' && num2 !== '' && sign !== '') {
        document.querySelector('.orange').removeAttribute('disabled')
    }
});

