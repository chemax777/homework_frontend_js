/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф з id="test"
focus blur input

2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість символів. Скільки символів має бути в інпуті, зазначається в атрибуті data-length. Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.

4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/

/*Завдання 1*/

const input = document.querySelector('.input-text');

input.addEventListener('blur', () => {
    document.querySelector('#test').textContent = input.value;
})


/*Завдання 2*/

const [...inputs] = document.querySelectorAll('.input-task-2');

inputs.forEach((inp) => {
    inp.addEventListener('blur', () => {
        inp.className = '';
        if (inp.dataset.length == inp.value.length) {
            inp.classList.add('border-green')
        } else {
            inp.classList.add('border-red')
        }
    })
})

/*Завдання 4*/

const inputPrice = document.querySelector('.price__input')
const span = document.createElement('span')
const button = document.createElement('button')
const message = document.createElement('div')
message.textContent = 'Please enter correct price';
button.textContent = 'X'


inputPrice.addEventListener('focus', () => {
    inputPrice.classList.add('border-green')
})

inputPrice.addEventListener('blur', () => {
    inputPrice.classList.remove('border-green');
    span.textContent = inputPrice.value;
    inputPrice.classList.add('green-text')

    if (inputPrice.value < 0) {
        inputPrice.classList.add('border-red')
        inputPrice.after(message)
        span.remove();
        button.remove();
    } else {
        document.querySelector('.price__label').append(span)
        document.querySelector('.price__label').append(button)
        inputPrice.classList.remove('border-red')
        inputPrice.classList.add('border-green')
        message.remove();
    }
})

button.addEventListener('click', ()=>{
    span.remove();
    button.remove();
})
