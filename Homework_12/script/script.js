const [...buttons] = document.querySelectorAll('.btn');


document.body.addEventListener('keypress', (e) => {
    buttons.forEach((elem => {
        if (elem.classList.contains('active')) {
            elem.classList.remove('active')
        }
    }))

    buttons.forEach((elem => {
        if (e.key != 'Enter') {
            if (elem.textContent === e.code[3]) {
                elem.classList.add('active')
            }
        } else if (elem.textContent === 'Enter') {
            elem.classList.add('active')
        }
    }))
})
