// Виселица
/*
const words = [
    "программа",
    "макака",
    "прекрасный",
    "оладушек"
];
// Выбираем случайное слово
let word = words[Math.floor(Math.random() * words.length)];
// Создаем итоговый массив
let answerArray = [];
for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
}
let remainingLetters = word.length;
// Игровой цикл
while (remainingLetters > 0) {
    // Показываем состояние игры
    alert(answerArray.join(" "));
    // Запрашиваем вариант ответа
    let guess = prompt("Угадайте букву, или нажмите Отмена для выхода из игры.");
 if (guess === null) {
        // Выходим из игрового цикла
        break;
    } else if (guess.length !== 1) {
        alert("Пожалуйста, введите одиночную букву.");
    } else {
        // Обновляем состояние игры
        for (let j = 0; j < word.length; j++) {
            if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }
    // Конец игрового цикла
}
// Отображаем ответ и поздравляем игрока
alert(answerArray.join(" "));
alert("Отлично! Было загадано слово " + word);
*/


// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
// При вызове функция должна спросить у вызывающего имя и фамилию.
// Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
// Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре(например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser().Вызвать у пользователя функцию getLogin().Вывести в консоль результат выполнения функции.

// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
// Возьмите выполненное задание выше(созданная вами функция createNewUser()) и дополните ее следующим функционалом:
// При вызове функция должна спросить у вызывающего дату рождения(текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре,
// соединенную с фамилией(в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.


class User {
    constructor(firstName, lastName, birthday) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }
    static createNewUser() {
        return new User(prompt('First name'), prompt('Surname'), prompt('Birthday dd.mm.yyyy'));
    }
    getLogin() {
        let arrFirst = this.firstName.split('');
        let surnameString = this.lastName.toLowerCase();
        let login = arrFirst[0].toLowerCase() + surnameString;
        return login;
    }
    getAge() {
        let userYear = this.birthday.split('.');
        let date = new Date();
        let userAge = date.getFullYear() - userYear[2];
        return userAge;
    }
    getPassword() {
        let arrFirst = this.firstName.split('');
        let surnameString = this.lastName.toLowerCase();
        let userYear = this.birthday.split('.');
        let userPassword = arrFirst[0].toUpperCase() + surnameString + userYear[2];
        return userPassword;
    }
}

const user1 = User.createNewUser();
console.log(user1);
console.log(user1.getLogin());
console.log(user1.getAge());
console.log(user1.getPassword());



// Реализовать функцию фильтра массива по указанному типу данных.

// Написать функцию filterBy(), которая будет принимать в себя 2 аргумента.Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом.То есть, если передать массив['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив[23, null].

function filterBy(array, data) {
    newArray = [];
    for (let i = 0; i < array.length; i++) {
        if (typeof array[i] === data) {
            continue;
        } else {
            newArray.push(array[i]);
        }
    }
    return newArray;
}

const arr = ['hello', 'world', 23, '23', null];
console.log(filterBy(arr, 'string'))
