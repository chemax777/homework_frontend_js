
// 2. Створіть програму секундомір.
// * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ: ММ: СС
// * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

const buttonStart = document.querySelector('#start');
const buttonStop = document.querySelector('#stop');
const buttonReset = document.querySelector('#reset');
const seconds = document.querySelector('#watch-second');
const minutes = document.querySelector('#watch-minute');
const hours = document.querySelector('#watch-hour');
let sec = 0;
let min = 0;
let hour = 0;
let timer;
let press = 0;

let startTimer = () => {
    sec++;
    if (sec > 59) {
        min++
        sec -= 60;
    }
    if (min > 59) {
        hour++
        min -= 60;
    }
    if (sec < 10) {
        seconds.textContent = `0${sec}`;
    } else {
        seconds.textContent = sec;
    }
    if (min < 10) {
        minutes.textContent = `0${min}`;
    } else {
        minutes.textContent = min;
    }
    if (hour < 10) {
        hours.textContent = `0${hour}`;
    } else {
        hours.textContent = hour;
    }
}

let clearTimer = () => {
    sec = 0;
    min = 0;
    hour = 0;
    seconds.textContent = '00';
    minutes.textContent = '00';
    hours.textContent = '00';
}

buttonStart.onclick = () => {
    document.querySelector('.stopwatch-display').classList.add('green')
    document.querySelector('.stopwatch-display').classList.remove('red')
    document.querySelector('.stopwatch-display').classList.remove('silver')
    press++;
    if(press > 1){
        startTimer;
    } else {
        timer = setInterval(startTimer, 1000)
    }
}
buttonStop.onclick = () => {
    document.querySelector('.stopwatch-display').classList.remove('green')
    document.querySelector('.stopwatch-display').classList.remove('silver')
    document.querySelector('.stopwatch-display').classList.add('red')
    clearInterval(timer)
    press = 0;
}
buttonReset.onclick = () => {
    document.querySelector('.stopwatch-display').classList.remove('green')
    document.querySelector('.stopwatch-display').classList.remove('red')
    document.querySelector('.stopwatch-display').classList.add('silver')
    clearTimer();
}