// Реалізуйте програму перевірки телефону
// * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
// * Користувач повинен ввести номер телефону у форматі 000 - 000 - 00 - 00
// * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, відобразіть її в діві до input.

const input = document.createElement('input');
const saveButton = document.createElement('input');
input.setAttribute('placeholder', 'Введіть телефон у форматі ХХХ-ХХХ-ХХ-ХХ');
input.setAttribute('title', 'Введіть телефон у форматі ХХХ-ХХХ-ХХ-ХХ');
saveButton.setAttribute('type', 'button');
saveButton.setAttribute('value', 'Зберегти');
let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;

document.body.append(input);
document.body.append(saveButton);

const error = document.createElement('div');
error.textContent = 'Введено невірний номер телефону! Введіть телефон у форматі XXX-XXX-XX-XX';
error.classList.add('red');

saveButton.onclick = () => {
    if (pattern.test(input.value)) {
        document.body.classList.add('green');
        document.location = 'https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg';
    } else {
        input.before(error);
    }
}