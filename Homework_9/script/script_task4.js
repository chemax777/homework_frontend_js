// Слайдер
// Створіть слайдер кожні 3 сек змінюватиме зображення
// Зображення для відображення
// https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
// https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
// https://naukatv.ru/upload/files/shutterstock_418733752.jpg
// https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
// https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg

const images = [
    'https://images.aif.by/007/676/ffb8ae9ffaf77002c8661a4828181232.jpg',
    'https://www.interfax.ru/ftproot/photos/photostory/2017/08/10/700tw.jpg',
    'https://api.starylev.com.ua/storage/files/blog/kosmonavtuka_1.jpg',
    'https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg',
    'https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg'
]
let nextBtn = document.querySelector('.next');
let prevBtn = document.querySelector('.prev')
let slider = document.querySelector('.slider');
let img = slider.querySelector('img');
let counter = 1;

img.src = images[0];

let changeImg = () => {
    img.src = images[counter]
    counter++
    if (counter === images.length) {
        counter = 0;
    }
    
}

let prevImg = () => {
    img.src = images[counter]
    counter--
    if (counter < 0) {
        counter = images.length-1;
    }
   
}

nextBtn.onclick = changeImg;
prevBtn.onclick = prevImg;
setInterval(changeImg, 3000);

