// Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
//     Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
//     На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
//     сторінок показувати нові коментарі. 
//     з коментарів виводити : 
//     "id": 1,
//     "name"
//     "email"
//     "body":

const ajax = new XMLHttpRequest();
const paggination = document.querySelector('.paggination');
const [...pagginationButtons] = document.querySelectorAll('.pag-btn-digit')

ajax.open("get", "https://jsonplaceholder.typicode.com/comments");
ajax.send();
ajax.addEventListener("readystatechange", () => {
    if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
        const data = JSON.parse(ajax.response);

        data.slice(0, 10)
            .map((el) => {
                let { id, name, email, body } = el
                showComments(id, name, email, body)
            })

        paggination.addEventListener('click', activeBtn)

        paggination.addEventListener('click', e => {
            let curPage = 0;
            let showItems = 10;
            let startIndex = 0;
            let endIndex = 10;

            document.querySelector('.comments').innerHTML = '';
            if (e.target.classList.contains('pag-btn-digit')) {
                curPage = e.target.textContent - 1
            } else if (e.target.classList.contains('pag-btn-next')) {
                document.querySelector('.pag-btn-prev').classList.remove('unvisible');
                pagginationButtons.forEach(el => {
                    el.textContent = +el.textContent + 1;
                    curPage = document.querySelector('.active').textContent - 1;
                    if (el.textContent >= data.length / showItems) {
                        document.querySelector('.pag-btn-next').classList.add('unvisible')
                    }
                })
            } else if (e.target.classList.contains('pag-btn-prev')) {
                document.querySelector('.pag-btn-next').classList.remove('unvisible');
                pagginationButtons.forEach(el => {
                    el.textContent = +el.textContent - 1;
                    curPage = document.querySelector('.active').textContent - 1;
                    if (el.textContent < 2) {
                        document.querySelector('.pag-btn-prev').classList.add('unvisible')
                    }
                })
            }

            startIndex = curPage * showItems;
            endIndex = startIndex + showItems;

            data.slice(startIndex, endIndex)
                .map((el) => {
                    let { id, name, email, body } = el
                    showComments(id, name, email, body)
                })
        })
    } else if (ajax.readyState === 4) {
        throw new Error(`Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`)
    }
})

function activeBtn(e) {
    if (!e.target.classList.contains('pag-btn-digit')) return
    document.querySelectorAll(".pag-btn-digit").forEach(el => {
        if (el.classList.contains('active')) {
            el.classList.remove('active')
        }
    })
    e.target.classList.add('active')
}

function showComments(id, name, email, bodyComent) {
    document.querySelector('.comments').insertAdjacentHTML('beforeend', `
        <div class="comment">
            <p class="comment__id">${id}</p>
            <h2 class="comment__name"> ${name}</h2>
            <p class="comment__email">
                <a href="mailto:${email}">${email}</a>
            </p>
            <p class="comment__body">${bodyComent}</p>
        </div>
    `)
}



