// #1
const arr1 = ['a', 'b', 'c'];
const arr2 = arr1.concat(1, 2, 3);
console.log(arr2);

//#2
arr1.push(1, 2, 3);
console.log(arr1);
//#3
arr3 = [1, 2, 3];
arr3.reverse();
console.log(arr3);

//#4
const arr4 = [1, 2, 3];
arr4.push(4, 5, 6);
console.log(arr4);

//#5
const arr5 = [1, 2, 3];
arr5.unshift(4, 5, 6);
console.log(arr5);

//#6
const arr6 = ['js', 'css', 'jq'];
document.write(arr6[0]);

//#7
arr7 = [1, 2, 3, 4, 5];
const arr8 = arr7.slice(0, 3);
console.log(arr8);

//#8
const arr9 = [1, 2, 3, 4, 5];
arr9.splice(1, 2);
console.log(arr9);

//#9
const arr10 = [1, 2, 3, 4, 5];
arr10.splice(2, 0, 10);
console.log(arr10);

//#10
const arr11 = [3, 4, 1, 2, 7];
arr11.sort();
console.log(arr11);

//#11
const arr12 = ['Привіт, ', 'світ', '!']
arr12[1] = 'мир';
document.write("<br>" + arr12.join(''));

//#12
const arr13 = ['Привіт, ', 'світ', '!'];
arr13[0] = 'Поки';
console.log(arr13);

//#13
const arr14 = [1, 2, 3, 4, 5];
const arrNew = new Array(1, 2, 3, 4, 5);

//#14
var arr = {
    'ru': ['блакитний', 'червоний', 'зелений'],
    'en': ['blue', 'red', 'green'],
};
const arrLang = [arr.ru[0], arr.en[0]]
document.write('<br>' + arrLang.join(' '));

//#15
const arr15 = ['a', 'b', 'c', 'd'];
const arr15New = [arr15[0] + '+' + arr15[1], arr15[2] + '+' + arr15[3]];
document.write('<br>' + arr15New.join(', ') + '<br>');

//#16
userArr = [];
qArr = parseInt(prompt('Введіть кількість елементів масиву'))

for (i = 0; i < qArr; i++){
    userArr.push(i);
}

//#17
for (i = 0; i < userArr.length; i++){
    if (userArr[i] % 2 === 0) {
        document.write(`<span style="background: red;">${userArr[i]}</span><br>`)
    } else {
        document.write(`<p>${userArr[i]}</p>`)
    }
}

//#18
var vegetables = ['Капуста', 'Ріпа', 'Редиска', 'Морковка'];
const str1 = vegetables.join(', ');
document.write(str1);