const loginPage = `
<div class="wrapper">
        <div class="left-side">
            <div class="left-side__img">
                <img src="./img/login1.png" alt="login1">
            </div>
        </div>
        <div class="right-side">
            <div class="login-form">
                <h2 class="login-form__header">Login</h2>
                <form>
                    <div class="login-form__email">
                        <label for="email">Email</label>
                        <input type="mail" id="email">
                    </div>
                    <div class="login-form__password">
                        <label for="password">Password</label>
                        <input type="password" id="password">
                    </div>
                    <div class="login-form__not-user">
                        Not a user? <a href="./registration/registration.html">Register now</a>
                    </div>
                    <div>
                        <button class = "login-form__login-btn">LOGIN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
`

const registrationPage = `
<div class="wrapper">
        <div class="left-side">
            <div class="left-side__picture">
                <img src="../img/login2.png" alt="login2">
            </div>
        </div>
        <div class="right-side">
            <div class="reg-form">
                <h2 class="reg-form__header">Registration form</h2>
                <form>
                    <div class="reg-form__input">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name">
                    </div>
                    <div class="reg-form__input">
                        <label for="date">Date of Birth</label>
                        <input type="date" name="birth" id="birth">
                    </div>
                    <div class="reg-form__input">
                        <label for="fmname">Father’s/Mother’s Name</label>
                        <input type="text" name="fmname" id="fmname">
                    </div>
                    <div class="reg-form__input">
                        <label for="email">Email</label>
                        <input type="mail" name="email" id="email">
                    </div>
                    <div class="reg-form__input">
                        <label for="fmname">Mobile No.</label>
                        <input type="phone" name="phone" id="phone">
                    </div>
                    <div class="reg-form__input">
                        <label for="password">Password</label>
                        <input type="password" name="password" id="password">
                    </div>
                    <div class="reg-form__input">
                        <label for="r-password">Re-enter password</label>
                        <input type="password" name="r-password" id="r-password">
                    </div>
                    <div class="reg-form__input">
                        <label for="h-phone">home Number</label>
                        <input type="phone" name="h-phone" id="h-phone">
                    </div>
                    <div class="reg-form__btn">
                        <button>SUBMIT</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
`

export {loginPage, registrationPage}