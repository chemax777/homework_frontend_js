import { registrationPage } from "./pages.js";
import { checkRegForm, writeUserInLocalStorage } from "./functions.js";

const body = document.querySelector("body");

body.insertAdjacentHTML("beforeend", registrationPage)

if (!localStorage.userStorage) {
    localStorage.userStorage = JSON.stringify([])
}

const formReg = document.querySelector('form')
const submitBtn = document.querySelector('button')


formReg.addEventListener("change", checkRegForm)
submitBtn.addEventListener("click", writeUserInLocalStorage)