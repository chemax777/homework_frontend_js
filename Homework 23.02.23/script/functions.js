function validate(pattern, value) {
    return pattern.test(value);
}

function checkRegForm(e) {
    if (e.target.name === "name" && validate(/^[a-zA-Z]{2,}$/gi, e.target.value)) {
        e.target.classList.remove('error');
    } else if (e.target.name === "birth") {
        e.target.classList.remove('error');
    } else if (e.target.name === "fmname" && validate(/^[a-zA-Z]{2,}$/gi, e.target.value)) {
        e.target.classList.remove('error');
    } else if (e.target.name === "email" && validate(/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/gi, e.target.value)) {
        e.target.classList.remove('error');
    } else if (e.target.name === "phone" && validate(/^\+380[0-9]{9}$/gi, e.target.value)) {
        e.target.classList.remove('error');
    } else if (e.target.name === "password" && validate(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/g, e.target.value)) {
        e.target.classList.remove('error');
    } else if (e.target.name === "r-password" && e.target.value === document.querySelector('#password').value) {
        e.target.classList.remove('error');
    } else if (e.target.name === "h-phone" && validate(/^[0-9]{6}$/gi, e.target.value)) {
        e.target.classList.remove('error');
    } else {
        e.target.classList.add('error');
    }
}

function writeUserInLocalStorage(e) {
    e.preventDefault()
    const storage = JSON.parse(localStorage.userStorage);
    const user = {
        name: '',
        dateOfBirth: '',
        fmName: '',
        email: '',
        phone: '',
        password: '',
        hPhone: ''
    }
    const [...inputs] = document.querySelectorAll('input')
    const userValues = inputs.map(el => {
        return el.value
    })
    const inputsClasses = inputs.map(el => {
        return el.classList.value
    })

    if (inputsClasses.includes('error')) {
        document.querySelector('.reg-form').insertAdjacentHTML("beforeend", `
        <span class="error-msg">Please, check your data!</span>
        `)
    } else {
        user.name = userValues[0];
        user.dateOfBirth = userValues[1];
        user.fmName = userValues[2];
        user.email = userValues[3];
        user.phone = userValues[4];
        user.password = userValues[5];
        user.hPhone = userValues[7];

        if (user.name === '' || user.dateOfBirth === '' || user.fmName === '' || user.email === '' || user.password === '' || user.hPhone === '') {
            document.querySelector('.reg-form').insertAdjacentHTML("beforeend", `
        <span class="error-msg">Please, check your data!</span>
        `)
        } else {
            storage.push(user)
            localStorage.userStorage = JSON.stringify(storage);
            alert("Registration successful! Press OK to redirect on Login page")
            document.location = '../index.html'
        }
    }
}

function readUserFromLocalStorage(e) {
    const users = JSON.parse(localStorage.userStorage)
    const loginInput = document.querySelector('.login-form__email input')
    const passwordInput = document.querySelector('.login-form__password input')
    const errorMsg = document.createElement('div')

    errorMsg.textContent = "Incorrect login or password!"
    errorMsg.classList.add('error-msg')

    if (!e.target.classList.contains('login-form__login-btn')) return;

    e.preventDefault()

    users.forEach(el => {
        let { email, password } = el
        if (loginInput.value === email && passwordInput.value === password) {
            alert("Login successful!")
            loginInput.value = "";
            passwordInput.value = "";
        } else {
            document.querySelector('.login-form').append(errorMsg);
        }
    });
}

export { checkRegForm, writeUserInLocalStorage, readUserFromLocalStorage }