import { loginPage } from "./pages.js";
import {readUserFromLocalStorage} from "./functions.js"

const body = document.querySelector("body");

body.insertAdjacentHTML("beforeend", loginPage)

const loginForm = document.querySelector('.login-form')

loginForm.addEventListener("click", readUserFromLocalStorage)


