// При загрузке страницы - показать на ней кнопку с текстом "Нарисовать круг".Данная кнопка должна являться единственным контентом в теле HTML документа, весь остальной контент должен быть создан и добавлен на страницу с помощью Javascript
// При нажатии на кнопку "Нарисовать круг" показывать одно поле ввода - диаметр круга. При нажатии на кнопку "Нарисовать" создать на странице 100 кругов(10 * 10) случайного цвета.При клике на конкретный круг - этот круг должен исчезать, при этом пустое место заполняться, то есть все остальные круги сдвигаются влево.

let buttonCircle = document.getElementById('circle-btn');

let inputWidth = document.createElement('input');
inputWidth.setAttribute('type', 'text');
inputWidth.setAttribute('placeholder', 'Введите диаметр круга в px');
inputWidth.setAttribute('id', 'input-width');
let drawButton = document.createElement('input');
drawButton.setAttribute('type', 'button');
drawButton.setAttribute('value', 'Нарисовать');
drawButton.setAttribute('id', 'draw-button');

buttonCircle.onclick = () => {  
    document.body.append(inputWidth);
    document.body.append(drawButton);
}

drawButton.onclick = () => {
    let circleWidth = inputWidth.value;
    let container = document.createElement('div');
    container.className = 'container'
    document.body.append(container);
    const circles = [];
    for (i = 0; i < 100; i++) {
        let circle = document.createElement('div');
        circle.className = 'circle';
        circle.style.width = `${circleWidth}px`
        circle.style.height = `${circleWidth}px`
        circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`
        circles.push(circle)

        circle.onclick = () => {
            circle.remove();
        }
    }
    container.append(...circles)
}


