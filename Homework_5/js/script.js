// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".Создать в объекте вложенный объект - "Приложение".Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".Создать методы для заполнения и отображения документа.
const doc = {
    header: 'header',
    body: 'body',
    footer: 'footer',
    date: 'date',
    application: {
        appHeader: '',
        appBody: '',
        appFooter: '',
        appDate: '',
    },
    write: function () {
        doc.application.appHeader = prompt('Введите заголовок документа:')
        doc.application.appBody = prompt('Введите тело документа:')
        doc.application.appFooter = prompt('Введите футер документа:')
        doc.application.appDate = prompt('Введите дату документа:')
    },
    show: function () {
        document.getElementById('main').innerHTML = `
        <h1>${doc.application.appHeader} </h1>
        <p>${doc.application.appBody}</p> 
        <p>${doc.application.appFooter}</p> 
        <p>${doc.application.appDate}</p>
        `
    },
}

doc.write();
doc.show();

