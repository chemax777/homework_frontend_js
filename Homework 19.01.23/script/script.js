// Згенерувати теги теги через javascript. Додати на сторінку семантичні теги та метагеги опису сторінки.
//  прописати стилі для для елементів використовуючи css id та класи
//  при натиску на тег ми можемо доти будь-який контент і він зберігається в тегу

function createTag(tag, className) {
    tag = document.createElement(tag);
    tag.classList.add(className)
    return tag
}

const header = createTag('header', 'header');
const main = createTag('div', 'main');
const footer = createTag('footer', 'footer')

document.body.append(header);
header.append(createTag('nav', 'header__nav'));

document.body.append(main);
main.append(createTag('section', 'section-left'), createTag('section', 'section-right'));

document.querySelector('.section-left').append(
    createTag('header', 'section-left__header'),
    createTag('article', 'section-left__article1'),
    createTag('article', 'section-left__article2'),
    createTag('footer', 'section-left__footer')
);

document.querySelector('.section-right').append(
    createTag('header', 'section-right__header'),
    createTag('nav', 'section-right__nav')
);

document.querySelector('.section-left__article1').insertAdjacentHTML('beforeend', `
    <header class="article1__header"></header>
    <p class="arcticle1__p1"></p>
    <p class="arcticle1__p2"></p>
    <aside class="arcticle1__aside"></aside>
    <footer class="arcticle1__footer"></footer>`
);

document.querySelector('.section-left__article2').insertAdjacentHTML('beforeend', `
    <header class="article2__header"></header>
    <p class="arcticle2__p"></p>
    <p class="arcticle2__p"></p>
    <footer class="arcticle2__footer"></footer>`
);

document.body.append(footer);

document.body.addEventListener('click', (e) => {
    e.target.contentEditable = true
})

