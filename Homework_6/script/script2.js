// Реалізуйте клас Worker(Працівник), який матиме такі властивості: name(ім'я), surname (прізвище),
// rate(ставка за день роботи), days(кількість відпрацьованих днів).
// Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
// Зарплата - це добуток(множення) ставки rate на кількість відпрацьованих днів days.

class Worker {
    constructor(name, surname, rate, days) {
        this.name = name;
        this.surname = surname;
        this.rate = rate;
        this.days = days;
    }
    getSalary() {
        return this.rate * this.days
    }
}

worker1 = new Worker("Jonh", "Smith", 150, 30);
console.log(worker1.getSalary())


// Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
// який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
// який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
// та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.

class MyString {
    constructor(string) {
        this.string = string
    }
    reverse() {
        return this.string.split('').reverse().join('')
    }
    ucFirst() {
        const arr = this.string.split('');
        const firstLetter = arr[0].toUpperCase();
        const anotherWord = arr.slice(1).join('')
        return firstLetter + anotherWord
    }
    ucWords() {
        const arr = this.string.split(' ');
        return arr
            .map(
                (word, index) => word[0].toUpperCase() + word.slice(1)
            )
            .join(' ');
    }
}

const string1 = new MyString("max");
const string2 = new MyString("maxim gres");
console.log(string1.reverse());
console.log(string1.ucFirst());
console.log(string2.ucWords());

// Створіть клас Phone, який містить змінні number, model і weight.
// Створіть три екземпляри цього класу.
// Виведіть на консоль значення їх змінних.
// Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    receiveCall(name) {
        console.log(`Телефонує ${name}`)
    }
    getNumber() {
        return this.number;
    }
}

const phone1 = new Phone(380669503766, 'iPhone', 350);
const phone2 = new Phone(380950375521, 'Samsung', 375);
const phone3 = new Phone(380503558664, 'Xiaomi', 310);

for (let elem in phone1) {
    console.log(`${elem}: ${phone1[elem]}`)
}
for (let elem in phone2) {
    console.log(`${elem}: ${phone2[elem]}`)
}
for (let elem in phone3) {
    console.log(`${elem}: ${phone3[elem]}`)
}
phone1.receiveCall('Max')
console.log(phone1.getNumber());

// Створити клас Car, Engine та Driver.
// Клас Driver містить поля - ПІБ, стаж водіння.
// Клас Engine містить поля – потужність, виробник.
// Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine.Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч".А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

class Driver {
    constructor(fullName, experience) {
        this.fullName = fullName;
        this.experience = experience;
    }
    toString() {
        return `Driver fullname: ${this.fullName}, Driver experience: ${this.experience} years`
    }
}

class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }
    toString() {
        return `Power: ${this.power}, Company: ${this.company}`
    }
}

class Car {
    constructor(marka, carClass, weight, driver, engine) {
        this.marka = marka;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }
    start() {
        document.getElementById('car').innerHTML = `<p>Поїхали</p>`
    }
    stop() {
        document.getElementById('car').innerHTML = `<p>Зупиняємося</p>`
    }
    turnRight() {
        document.getElementById('car').innerHTML = `<p>Поворот праворуч</p>`
    }
    turnLeft() {
        document.getElementById('car').innerHTML = `<p>Поворот ліворуч</p>`
    }
    toString() {
        return `Marka: ${this.marka}, Class: ${this.carClass}, Weight: ${this.weight} kg, Driver info: ${this.driver}, Engine info: ${this.engine}`
    }
}
const driverMazda = new Driver("Ivan Ivanov", 2)
const engineMazda = new Engine(1600, "Galo40");
const mazdaCar = new Car('Mazda', 'S', 3000, driverMazda, engineMazda);

console.log(mazdaCar.toString())

// Створити похідний від Car клас - Lorry(вантажівка), що характеризується також вантажопідйомністю кузова.
// Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю

class Lorry extends Car {
    constructor(marka, carClass, weight, driver, engine, carrying) {
        super(marka, carClass, weight, driver, engine)
        this.carrying = carrying;
    }
    toString() {
        return `Marka: ${this.marka}, Class: ${this.carClass}, Weight: ${this.weight} kg, Driver info: ${this.driver}, Engine info: ${this.engine}, Carrying: ${this.carrying} kg`
    }
}
const engineLorry = new Engine(2500, 'CToS')
const driverLorry = new Driver('Jonh Smith', 10)
const lorryCar = new Lorry('Tata', 'A', 4000, driverLorry, engineLorry, 5000)
console.log(lorryCar.toString())

class SportCar extends Car {
    constructor(marka, carClass, weight, driver, engine, speed) {
        super(marka, carClass, weight, driver, engine)
        this.speed = speed;
    }
    toString() {
        return `Marka: ${this.marka}, Class: ${this.carClass}, Weight: ${this.weight} kg, Driver info: ${this.driver}, Engine info: ${this.engine}, Speed: ${this.speed} km/h`
    }
}

const engineSportCar = new Engine(3000, 'EA');
const driverSportCar = new Driver('Jack Richer', 15);
const sportCarCar = new SportCar('Posche', 'S', 1500, driverSportCar, engineSportCar, 320)
console.log(sportCarCar.toString())

// Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
// Клас Animal містить змінні food, location і методи makeNoise, eat, sleep.Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
// Dog, Cat, Horse перевизначають методи makeNoise, eat.
// Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.
// Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal).Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
// У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас.У циклі надсилайте їх на прийом до ветеринара.

class Animal {
    constructor(food, location) {
        this.food = food;
        this.location = location;
    }
    makeNoise() {
        console.log("Така тварина спить")
    }
    eat() {
        console.log(`Тварина їсть ${this.food}`)
    }
    sleep() {
        console.log('Зараз тварина спить')
    }
}

class Dog extends Animal {
    constructor(animalType, name, age, food, location) {
        super(food, location);
        this.animalType = animalType
        this.name = name;
        this.age = age;
    }
    makeNoise() {
        console.log("Собака спить")
    }
    eat() {
        console.log(`Собака їсть ${this.food}`)
    }
}

class Cat extends Animal {
    constructor(animalType, name, age, food, location) {
        super(food, location);
        this.animalType = animalType;
        this.name = name;
        this.age = age;
    }
    makeNoise() {
        console.log("Кот спить")
    }
    eat() {
        console.log(`Кот їсть ${this.food}`)
    }
}

class Horse extends Animal {
    constructor(animalType, name, age, food, location) {
        super(food, location);
        this.animalType = animalType;
        this.name = name;
        this.age = age;
    }
    makeNoise() {
        console.log("Кінь спить")
    }
    eat() {
        console.log(`Кінь їсть ${this.food}`)
    }
}
const dog = new Dog('Собака', 'Рекс', 2, 'Мясо', 'Україна');
const cat = new Cat('Кот', 'Тіша', 4, 'Риба', 'Польща');
const horse = new Horse('Кінь', 'Вірний', 5, 'Морква', 'Німеччина')

class Vet {
    static treatAnimal(animal) {
       console.log(`${animal.animalType} ${animal.name} їсть ${animal.food}. Місце знаходження - ${animal.location}`)
    }
    static main() {
        const animalArr = [dog, cat, horse];
        for (let i = 0; i < animalArr.length; i++){
            this.treatAnimal(animalArr[i])
        }
    }
}

Vet.main();
