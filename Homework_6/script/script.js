// CW
/*
function Calculator() { }

Calculator.prototype.read = function () {
    this.num1 = parseFloat(prompt('Введите первое число', 0));
    this.num2 = parseFloat(prompt('Введите второе число', 0));
}

Calculator.prototype.sum = function () {
    return this.num1 + this.num2;
}

Calculator.prototype.mul = function () {
    return this.num1 * this.num2;
}

const calc = new Calculator();
calc.read();

alert(`Сумма чисел равна ${calc.sum()}`);
alert(`Произведение чисел равно ${calc.mul()}`);
*/
// Задача 1
/*
function Human(name, age) {
    this.name = name;
    this.age = age
}

const human1 = new Human("Max", 27);
const human2 = new Human("Vova", 26);
const human3 = new Human("Masha", 19);
const human4 = new Human("Sveta", 22);
const human5 = new Human("Alex", 33);

const humans = [human1, human2, human3, human4, human5];

const sortByAgeDesc = (a, b) => a.age > b.age ? -1 : 1;

console.dir(humans.sort(sortByAgeDesc));
*/

// Задача 2
/*
function Human(name, age, sex, hobbie) {
    this.name = name;
    this.age = age;
    this.sex = sex;
    this.hobbie = hobbie
}

Human.prototype.sayHi = function () {
    document.write(`Hello, ${this.name} <br/>`)
}
Human.prototype.getAge = function () {
    document.write(`Your age is ${this.age} <br/>`)
}
Human.prototype.getSex = function () {
    document.write(`You are ${this.sex} <br/>`)
}

const user = new Human("Max", 27, "Male", "Swiming");
console.dir(user);
user.sayHi();
user.getAge();
user.getSex();
*/



